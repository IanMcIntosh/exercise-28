using System;

namespace exercise_28
{
    class Person
    {
        //fpcm

        private string Name;
        private int Age;

        public string NameProperty
        {
            get{return Name;}
            set{Name = value;}
        }

        public int AgeProperty
        {
            get{return Age;}
            set{Age = value;}
        }

        public Person(){}

        public Person(string _name, int _age)
        {
            Name = _name;
            Age = _age;
        }

        public void SayHello()
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine($"My name is {NameProperty} and my age is {AgeProperty}");
        }
    }
}
 