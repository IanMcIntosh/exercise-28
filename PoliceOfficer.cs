using System;

namespace exercise_28
{
    class PoliceOfficer : Person
    {
        private int PoliceId;

        public int PIProperty
        {
            get {return PoliceId;}
            set {PoliceId = value;}
        }

        public PoliceOfficer() {}

        public PoliceOfficer(int _pi)
        {
            PoliceId = _pi;
        }

        public PoliceOfficer(int _pi, string _name, int _age)
        {
            PoliceId = _pi;
            NameProperty = _name;
            AgeProperty = _age;
        }

        public void Arrest()
        {
            Console.WriteLine("I am going to Arrest you for burgularing");
        }
    }
}
