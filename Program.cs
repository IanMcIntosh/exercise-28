﻿using System;

namespace exercise_28
{
    class Criminal
    {
        public void Burglar()
        {
            Console.WriteLine($"I am Burglaring something");
        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            var p1 = new Person();
            p1.NameProperty = "Jake";
            p1.AgeProperty = 19;
            p1.SayHello();            

            var p2 = new Person("Lisa", 23);
            p2.SayHello();
            

            Console.WriteLine(p1.NameProperty);
            Console.WriteLine(p2.NameProperty);


            var police1 = new PoliceOfficer(123456, "John", 45);

            var police2 = new PoliceOfficer(123456);

            var police3 = new PoliceOfficer();

            police1.Arrest();
            police2.Arrest();
            police3.Arrest();

           var Burglar1 = new Criminal();
           Burglar1.Burglar(); 
        }
    }
}
